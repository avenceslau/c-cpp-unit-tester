//
//   internal_tester.c
//
//  Created by André Venceslau on 12/08/2021.
//

#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include "tester.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"

#define ANSI_BYEL "\e[1;33m"
#define ANSI_BGRN "\e[1;32m"
#define ANSI_BRED "\e[1;31m"

#define ANSI_COLOR_RESET   "\x1b[0m"

#if defined(NCOLOR)
    #define NOCOLOR 1
#else
    #define NOCOLOR 0
#endif


#define SUCCESS "SUCCESS"
#define ERROR "ERROR"

#define CHECK_MALLOC(x) if(x == NULL) {printf("INTERNAL MALLOC ERROR ABORTING\n");exit(1);}

#define STATUS(boolean) ((boolean) ? SUCCESS : ERROR)
#define COLOR_RESET ((NOCOLOR != 1) ? ANSI_COLOR_RESET : "")

#define CHOOSE_COLOR_GR(boolean) ((boolean) ? ANSI_COLOR_GREEN : ANSI_COLOR_RED)
#define MESSAGE_COLOR(boolean) ((NOCOLOR != 1) ? CHOOSE_COLOR_GR(boolean) : "")

#define FILE_MSG_COLOR(boolean) ((NOCOLOR != 1) ? ANSI_COLOR_YELLOW : "")

#define PRINT_INFO(filename, line) " %sLINE%s: %d ::: %sFILE%s: %s", \
        FILE_MSG_COLOR(true), COLOR_RESET, line, \
        FILE_MSG_COLOR(true), COLOR_RESET, filename

#define ASSERT_PRINT "\n\t[%sASSERTION FAILED%s]", MESSAGE_COLOR(false), COLOR_RESET
        

#define MSG(msg, comparison) (((comparison == true) || strcmp(msg, "") == 0) ? "" : msg)


#define ERROR_PRINT(test_name, comparison, msg, line, filename) "\t\t%s Status: [%s%s%s] %s", test_name, \
            MESSAGE_COLOR(comparison), STATUS(comparison), COLOR_RESET, \
            MSG(msg, comparison)




typedef struct TestSuiteNode * TestSuiteNode;
typedef struct TestNode * TestNode;

typedef unsigned long ul;

struct Test{
    string test_name;
    bool result;
    char * error_msg;
    int line;
    string filename;
};

struct TestNode {
    Test test;
    TestNode next;
};

struct TestSuiteNode {
    TestSuite suite;
    TestSuiteNode next;
};

struct TestSuite {
    ul number_tests;
    string suite_name;
    TestNode head;
    TestNode tail;
};

TestSuiteNode root = NULL;
ul num_clusters = 0;

char * _format_msg(string fmt, va_list arguments);

TestSuiteNode _find_TestSuite(const char * suite_name){
    TestSuiteNode aux = root;
    while (aux != NULL) {
        if (strcmp(aux->suite->suite_name, suite_name) == 0) {
            return aux;
        }
        aux = aux->next;
    }
    return NULL;
}

void free_Suite(string cluster_name){
    TestSuiteNode aux = root;
    if (strcmp(root->suite->suite_name, cluster_name) == 0) {
        root = root->next;
        free(aux->suite);
        free(aux);
        num_clusters--;
        return;
    }
    while (aux->next != NULL) {
        if(strcmp(aux->next->suite->suite_name, cluster_name) == 0) {
            TestSuiteNode tmp = aux->next;
            aux->next = tmp->next;
            tmp->next = root;
            root = tmp;
            break;
        }
        aux = aux->next;
    }
    if (strcmp(root->suite->suite_name, cluster_name) == 0) {
        aux = root;
        root = root->next;
        free(aux->suite);
        free(aux);
        num_clusters--;
    }
    
    return;
}

void results_print(Test test){
#ifdef SILENT
    if (test->result) {
        return;
    }
#endif
    printf(ERROR_PRINT(test->test_name, test->result, test->error_msg, test->line, test->filename));
    if (test->result == false) {
        printf(PRINT_INFO(test->filename, test->line));
    }
    printf("\n");
}

void _assert(int line, const char * filename, bool comparison) {
    if (comparison == false) {
        printf(ASSERT_PRINT);
        printf(PRINT_INFO(filename, line));
        printf("\n");
    }
    return;
}

void delete_test(Test *test){
    free((*test)->error_msg);
    free((*test));
    *test = NULL;
}

void create_suite_node(TestSuite suite){
    TestSuiteNode node = (TestSuiteNode) malloc(sizeof(struct TestSuiteNode));
    CHECK_MALLOC(node);
    node->suite = suite;
    node->next = root;
    num_clusters++;
    root = node;
    return;
}

void _tests_create_suite(const char * name) {
    _TestSuite * suite = (TestSuite) malloc(sizeof(_TestSuite));
    CHECK_MALLOC(suite);
    create_suite_node(suite);
    
    suite->number_tests = 0;
    suite->head = NULL;
    suite->tail = NULL;
    suite->suite_name = name;
    return;
}

void run_test(Test node){
    results_print(node);
}

void add_to_Suite(TestNode node, string suite_name) {
    TestSuiteNode suite_node = _find_TestSuite(suite_name);
    TestSuite suite = suite_node->suite;
    if (suite_node == NULL) {
        return;
    }
    if (suite->head == NULL) {
        suite->head = node;
    } else {
        suite->tail->next = node;
    }
    suite->tail = node;
    suite->number_tests++;
    
}

void free_TestNode(TestNode node) {
    free(node->test->error_msg);
    free(node->test);
    free(node);
}

void _run_suite(const char * suite_name){
    TestSuiteNode suite_node = _find_TestSuite(suite_name);
    if (suite_node == NULL) {
        printf("\tTestSuite %s not found\n", suite_name);
        return;
    }
    printf("\n\tResults for TestSuite %s\n", suite_name);
    int number_successes = 0;
    TestNode aux = NULL;
    TestNode runner = suite_node->suite->head;
    while (runner != NULL) {
        run_test(runner->test);
        if(runner->test->result == true)
            number_successes++;
        aux = runner;
        runner = runner->next;
        if (aux != NULL)
            free_TestNode(aux);
    }
    string color = MESSAGE_COLOR((number_successes == suite_node->suite->number_tests));
    printf("\tSUMMARY: %s%d%s of %ld tests were successful\n\n", color,number_successes, COLOR_RESET, suite_node->suite->number_tests);
    free_Suite(suite_name);
    return;
}

TestNode create_test_node(Test test){
    TestNode node = (TestNode) malloc(sizeof(struct TestNode));
    node->next = NULL;
    node->test = test;
    return node;
}

char * _format_msg(const char * fmt, va_list arguments){
    char * message = NULL;
    vasprintf(&message, fmt, arguments);
    CHECK_MALLOC(message);
    return message;
}

void _create_test(int line, string filename, string suit_name, string test_name, bool comparison, string fmt, ...){
    Test new_test = (_Test *) malloc(sizeof(_Test));
    CHECK_MALLOC(new_test);
    TestNode new_node = create_test_node(new_test);
    CHECK_MALLOC(new_node);
    new_test->test_name = test_name;
    new_test->result = comparison;
    new_test->filename = filename;
    new_test->line = line;
    
    va_list arguments;
    va_start(arguments, fmt);
    new_test->error_msg = _format_msg(fmt, arguments);
    va_end(arguments);
    add_to_Suite(new_node, suit_name);
    return;
}
