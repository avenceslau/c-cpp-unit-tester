//
//  internal_tester.h
//
//  Created by André Venceslau on 12/08/2021.
//
#ifndef internal_tester_h
#define internal_tester_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>

/* private interface */

typedef const char * string;

typedef struct TestSuite _TestSuite;
typedef _TestSuite * TestSuite;

typedef struct Test _Test;
typedef _Test * Test;

void _tests_create_suite(const char * name);
void _assert(int line, const char * filename, bool comparison);
Test create_test(bool test, const char * name, const char * group, const char * fmt, ...);
void _run_suite(const char * suite_name);
void run_cluster(TestSuite cluster, bool silent);

void _create_test(int line, string filename, string suit_name, string test_name, bool comparison, string fmt, ...);

typedef _TestSuite * TestSuite;
typedef _Test * Test;

/* User interface */

#ifdef TESTMODE

#define TEST(suit_name, test_name, comparison, ...) \
_create_test(__LINE__, __FILE__, suit_name, test_name, comparison,  __VA_ARGS__)
#define SUITE(name) _tests_create_suite(name)
#define ASSERT(comparison) _assert(__LINE__,__FILE__, comparison)
#define SUITE_RESULTS(name) _run_suite(name)

#else

#define TEST(suit_name, test_name, comparison, ...)
#define SUITE(name)
#define ASSERT(comparison)
#define SUITE_RESULTS(name)

#endif

#ifdef __cplusplus
}
#endif

#endif /* internal_tester_h */
