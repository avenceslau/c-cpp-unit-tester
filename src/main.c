#include <stdio.h>

#include "tester.h"

int main(int argc, const char * argv[]) {
	ASSERT(1 == 3);
	const char * suite_name = "MySuite";
        
        //Creates a suite
        SUITE(suite_name); 
        const char * test_name = "Test1";
        
        //Create tests
        TEST(suite_name, test_name, 2+1 == 3, "2+1 = 3, there was an error"); 
        TEST(suite_name, "Test2", 2+2 == 3, "2+2 = 4, there was an error"); 

        //Show test results
        SUITE_RESULTS(suite_name);
	return 0;
}
