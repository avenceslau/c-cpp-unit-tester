# C-Cpp Unit Tester

Fast and simple to use unit tester for c/c++

To enable tests to be run the flag -D TESTMODE must be passed when compiling.
Also supports a silent mode with -D SILENT (only prints errors).
If your terminal or IDE does not support colors you can also add -D NCOLOR to remove colors.

Currently supports simple assertions, and test suite creations, for example: 

Assertions
    

        char * str = NULL;
        ASSERT(str == NULL); //On an error prints the name of the file and line where the error was found
    
Test suites
        
        const char * suite_name = "MySuite";
        
        //Creates a suite
        SUITE(suite_name); 
        const char * test_name = "Test1";
        
        //Create tests
        TEST(suite_name, test_name, 2+1 == 3, "2+1 = 3, there was an error"); 
        TEST(suite_name, "Test2", 2+2 == 3, "2+2 = 4, there was an error"); 

        //Show test results
        SUITE_RESULTS(suite_name);
    
A test can be created in any file as long as the suite has been created.
Showing the results destroys the Suite.

If there is a program crash test results will not be available as of yet...
 

